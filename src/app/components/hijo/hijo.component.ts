import { outputAst } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {



  constructor() { }
  
  mensaje: string = 'Mensaje del hijo al padre';
  mensaje2: string = 'Aqui el hijo, cambio';
  mensaje3: string = 'Tercer mensaje desde el hijo';
  mensaje4: string = 'cuarto mensaje desde el hijo';
  mensaje5: string = 'quinto mensaje desde el hijo'

  @Output() EventoMensaje = new EventEmitter<string>();
  @Output() EventoMensaje2 = new EventEmitter<string>();
  @Output() EventoMensaje3 = new EventEmitter<string>();
  @Output() EventoMensaje4 = new EventEmitter<string>();
  @Output() EventoMensaje5 = new EventEmitter<string>();

  ngOnInit(): void {
    this.EventoMensaje.emit(this.mensaje);
    this.EventoMensaje2.emit(this.mensaje2)
    this.EventoMensaje3.emit(this.mensaje3)
    this.EventoMensaje4.emit(this.mensaje4)
    this.EventoMensaje5.emit(this.mensaje5)
  }

}
